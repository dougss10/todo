package com.dougss.todo.dto;

import lombok.Data;

@Data
public class UserInputDTO {

    private String username;
    private String password;
}
